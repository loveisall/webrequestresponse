﻿using System;
using System.Net;
using System.IO;

using Newtonsoft.Json.Linq;

namespace Ejercicio1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Ingrese el nombre de la pelicula para saber su año de estreno");
            string Moviename = Console.ReadLine();

            WebRequest request = WebRequest.Create("http://www.omdbapi.com/?t="+Moviename);

            WebResponse response = request.GetResponse();

            Stream stream = response.GetResponseStream();

            StreamReader sr = new StreamReader(stream);

            JObject data = JObject.Parse(sr.ReadToEnd());

            string año   = (string)data["Year"];

            Console.WriteLine("La pelicula ha sido estrenada en el año: ["+ año + "].");
            Console.Write(data.Count);

            Console.ReadKey();
        }

    }
}
